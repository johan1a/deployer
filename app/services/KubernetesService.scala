package services

import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;
import javax.inject.{Inject, Provider}
import play.api.{Logger, MarkerContext}
import play.api.libs.json._
import scala.concurrent.{ExecutionContext, Future}

class KubernetesService @Inject()(config: play.api.Configuration)(implicit ec: ExecutionContext) {

  private val logger = Logger(getClass)

  private val masterNodeUrl = config.get[String]("kubernetes.master.url")

  private val kubernetesConfig =
    new ConfigBuilder().withMasterUrl(masterNodeUrl).build();

  private val client = new DefaultKubernetesClient(kubernetesConfig)

  def listPods()(implicit mc: MarkerContext): Unit = {
    logger.info(s"Kubernetes master is ${masterNodeUrl}")
    logger.info(client.namespaces().list().toString())
    val list = client.pods().inNamespace("default").list()
    logger.info(list.toString)
  }

  def patch(deploymentName: String, containerName: String, image: String)(
      implicit mc: MarkerContext): Unit = {

    try {
      logger.info(s"Updating deployment ${deploymentName} to image ${image}")

      client
        .apps()
        .deployments()
        .inNamespace("default")
        .withName(deploymentName)
        .edit()
        .editSpec()
        .editTemplate()
        .editSpec()
        .editContainer(0)
        .withImage(image)
        .endContainer()
        .endSpec()
        .endTemplate()
        .endSpec()
        .done()

      logger.info("Update completed")
    } catch {
      case e: Exception => logger.error("err", e)
    }
  }

}
