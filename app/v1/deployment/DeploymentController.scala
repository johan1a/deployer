package v1.deployment

import javax.inject.Inject

import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}

case class Deployment(deploymentType: String,
                      imageReference: String,
                      deploymentName: String,
                      containerName: String)

/**
 * Takes HTTP requests and produces JSON.
 */
class DeploymentController @Inject()(cc: PostControllerComponents)(implicit ec: ExecutionContext)
  extends PostBaseController(cc) {

  private val logger = Logger(getClass)

  implicit val deploymentReads: Reads[Deployment] = Json.reads[Deployment]

  def index: Action[AnyContent] = PostAction.async { implicit request =>
    logger.trace("index: ")
    postResourceHandler.list()
    postResourceHandler.find.map { posts =>
      Ok(Json.toJson(posts))
    }
  }

  def process: Action[AnyContent] = PostAction.async { implicit request =>
    Future {
      val json = request.body.asJson.get
      logger.info("got json: " + json.toString)
      val input = json.as[Deployment]
      postResourceHandler.create(input)
      Created("OK")
    }
  }

  def show(id: String): Action[AnyContent] = PostAction.async {
    implicit request =>
      logger.trace(s"show: id = $id")
      postResourceHandler.lookup(id).map { post =>
        Ok(Json.toJson(post))
      }
  }

}
