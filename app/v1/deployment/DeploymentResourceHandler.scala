package v1.deployment

import java.io.FileReader
import javax.inject.{Inject, Provider}
import play.api.{Logger, MarkerContext}
import play.api.libs.json._

import io.fabric8.kubernetes.api.model.NamespaceBuilder;
import io.fabric8.kubernetes.api.model.Pod;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;

import scala.concurrent.{ExecutionContext, Future}
import services.KubernetesService

/**
  * DTO for displaying post information.
  */
case class PostResource(id: String, link: String, title: String, body: String)

object PostResource {

  /**
    * Mapping to read/write a PostResource out as a JSON value.
    */
  implicit val format: Format[PostResource] = Json.format
}

/**
  * Controls access to the backend data, returning [[PostResource]]
  */
class DeploymentResourceHandler @Inject()(
    config: play.api.Configuration,
    routerProvider: Provider[DeploymentRouter],
    kubernetesService: KubernetesService,
    postRepository: DeploymentRepository)(implicit ec: ExecutionContext) {

  private val logger = Logger(getClass)

  def list(): Unit = {
    logger.info("list called")
    kubernetesService.listPods()
  }

  def create(input: Deployment)(implicit mc: MarkerContext): Deployment = {
    logger.info(s"input: $input")
    kubernetesService.patch(input.deploymentName, input.containerName, input.imageReference)
    input
  }

  def lookup(id: String)(implicit
                         mc: MarkerContext): Future[Option[PostResource]] = {
    val postFuture = postRepository.get(PostId(id))
    postFuture.map { maybePostData =>
      maybePostData.map { postData =>
        createPostResource(postData)
      }
    }
  }

  def find(implicit mc: MarkerContext): Future[Iterable[PostResource]] = {
    postRepository.list().map { postDataList =>
      postDataList.map(postData => createPostResource(postData))
    }
  }

  private def createPostResource(p: PostData): PostResource = {
    PostResource(p.id.toString, routerProvider.get.link(p.id), p.title, p.body)
  }

}
