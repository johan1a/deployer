package v1.deployment

import org.mockito.{ArgumentMatchers, Matchers, Mockito}
import org.scalatest.TestData
import org.scalatestplus.mockito._
import org.scalatestplus.play._
import org.scalatestplus.play.guice._
import play.api.Application
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.Json
import play.api.mvc.Result
import play.api.test.CSRFTokenHelper._
import play.api.test.Helpers._
import play.api.test._
import services._

import scala.concurrent.Future
import scala.language.implicitConversions


class PostRouterSpec extends PlaySpec with GuiceOneAppPerTest with MockitoSugar {

  val kubernetesServiceMock = mock[KubernetesService]

  implicit override def newAppForTest(testData: TestData): Application =
    new GuiceApplicationBuilder()
      .overrides(bind[KubernetesService].toInstance(kubernetesServiceMock))
      .build()

  "PostRouter" should {

    "render the list of posts" in {
      val request = FakeRequest(GET, "/api/deployments").withHeaders(HOST -> "localhost:9000").withCSRFToken
      val home: Future[Result] = route(app, request).get

      val posts: Seq[PostResource] = Json.fromJson[Seq[PostResource]](contentAsJson(home)).get
      posts.filter(_.id == "1").head mustBe (PostResource("1", "/api/deployments/1", "title 1", "blog post 1"))
    }

    "render the list of posts when url ends with a trailing slash" in {
      val request = FakeRequest(GET, "/api/deployments/").withHeaders(HOST -> "localhost:9000").withCSRFToken
      val home: Future[Result] = route(app, request).get

      val posts: Seq[PostResource] = Json.fromJson[Seq[PostResource]](contentAsJson(home)).get
      posts.filter(_.id == "1").head mustBe (PostResource("1", "/api/deployments/1", "title 1", "blog post 1"))
    }
  }

}
